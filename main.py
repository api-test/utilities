import json

from gitlab import Gitlab
from tudatalib import TUDatalib

from tudata_utils.gitlab_project import ProjectCreator
from tudata_utils.xml_reader import parse_xml_file

GIT_URL = 'https://gitlab.ulb.tu-darmstadt.de'


def make_gitlab_project(group, name, ci_keys, token):
    gl = ProjectCreator(token)
    gl.create_project(group, name)
    for k, v in ci_keys.items():
        gl.create_ci_key(k, v)

    with open('metadata.json', 'w') as f:
        json.dump([
            {
                'key': 'dc.title',
                'value': name,
                'language': None,
            }
        ], f, indent=2)

    gl.add_files('main', ['.gitlab-ci.yml', '.gitlab/hermes-ci.yml', 'metadata.json'])


def make_tudatalib_item(handle, name, email, password):
    tu = TUDatalib(email=email, password=password)
    tu.check_availability()
    collection = tu.get_handle(handle)
    item = collection.items.create()
    tu.logout()
    return item.id


def create_metadata(token, namespace, project):
    with Gitlab(GIT_URL, private_token=token) as gl:
        project = gl.projects.get(f'{namespace}/{project}')
        project.files.get(file_path='metadata.json')
        contributors = {}

        for person in project.repository_contributors():
            name = person['name']
            if name in contributors:
                continue
            contributors[name] = {
                'key': 'dc.contributor.author',
                'value': name,
                'language': None,
            }
        print(project)
        metadata = list(contributors.values())
        metadata.append({
            'key': 'dc.title',
            'value': project.name,
            'language': None,
        })

        with open('metadata.json', 'w') as json_file:
            json.dump(metadata, json_file, indent=2)


if __name__ == '__main__':
    ci_variables = {
        'HERMES_PUSH_TOKEN': '',
        'TUDATALIB_MAIL': '',
        'TUDATALIB_PSWD': ''
    }

    print('Parse XML file')
    initial_metadata = parse_xml_file('test.xml')

    print('Create TUdatalib item')
    item_id = make_tudatalib_item(
        initial_metadata['collection_handle'],
        initial_metadata['title'],
        ci_variables['TUDATALIB_MAIL'],
        ci_variables['TUDATALIB_PSWD']
    )
    ci_variables['tudatalib_item'] = item_id

    print('Create Gitlab project')
    make_gitlab_project(
        group=initial_metadata['gitlab_group'].split('/')[-1],
        name=initial_metadata['title'],
        ci_keys=ci_variables,
        token=ci_variables['HERMES_PUSH_TOKEN'],
    )
