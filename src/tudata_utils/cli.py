import argparse
import json
from gitlab import Gitlab

GIT_URL = 'https://gitlab.ulb.tu-darmstadt.de'


def _make_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-t',
        '--token',
        required=True,
    )
    parser.add_argument('--json_name')
    parser.add_argument('--namespace')
    parser.add_argument('--project')

    return parser


def create_metadata(token, namespace, project, json_name):
    with Gitlab(GIT_URL, private_token=token) as gl:
        project = gl.projects.get(f'{namespace}/{project}')
        contributors = {}

        for person in project.repository_contributors():
            name = person['name']
            if name in contributors:
                continue
            print(f'Add {name} to contributors')
            switch_name = name.split()
            switch_name = f'{switch_name[-1]}, {switch_name[0]}'
            contributors[name] = {
                'key': 'dc.contributor.author',
                'value': switch_name,
                'language': None,
            }
        print(project)
        metadata = list(contributors.values())
        print('Add project title to metadata')
        metadata.append({
            'key': 'dc.title',
            'value': project.name,
            'language': None,
        })

        with open(json_name, 'w') as json_file:
            json.dump(metadata, json_file, indent=2)


def main():
    parser = _make_parser()
    args = parser.parse_args()
    create_metadata(args.token, args.namespace, args.project, args.json_name)
