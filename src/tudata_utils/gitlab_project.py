from gitlab import Gitlab


GIT_URL = 'https://gitlab.ulb.tu-darmstadt.de'


class ProjectCreator:
    def __init__(self, token):
        self.gl = Gitlab(GIT_URL, private_token=token)
        self._project = None
        self._group = None
        self.group_id = None

    def create_group(self, name, path):
        grp_opts = {
            'name': name,
            'path': path,
        }
        self._group = self.gl.groups.create(grp_opts)

        return self._group

    def create_project(self, group, name):
        print(f'Create project {group} / {name}')
        group_item = self.gl.groups.list(search=group)[0]
        project_opts = {
            'name': name,
            'namespace_id': group_item.id,
            'initialize_with_readme': True,
        }
        self._project = self.gl.projects.create(project_opts)

        return self._project

    def create_branch(self, name, ref):
        branch_opts = {
            'branch': name,
            'ref': ref,
        }
        branch = self._project.branches.create(branch_opts)

        return branch

    def create_ci_key(self, name, value, masked=True):
        print(f'Create CI key {name}')
        key_opts = {
            'key': name,
            'value': value,
            'masked': masked,
        }

        var = self._project.variables.create(key_opts)

        return var

    def add_files(self, branch, files):
        print(f'Commit files {files}')
        commit_opts = {
            'branch': branch,
            'commit_message': 'add files',
            'actions': [],
        }
        for f in files:
            commit_opts['actions'].append(
                {
                    'action': 'create',
                    'file_path': str(f),
                    'content': open(f).read(),
                }
            )

        self._project.commits.create(commit_opts)
