import xml.etree.ElementTree as ET


def parse_xml_file(filename):
    with open(filename, 'r') as f:
        tree = ET.parse(f)

        metadata = {
            'title': '',
            'collection_handle': '',
            'gitlab_group': '',
            'research_field': '',
        }

        root = tree.getroot()
        metadata['title'] = root.find('title').text
        for element in root.findall('values'):
            for child in element:
                value_type = child.find('attribute').attrib['{http://purl.org/dc/elements/1.1/}uri']

                if value_type.endswith('tudatalib_collection'):
                    metadata['collection_handle'] = child.find('text').text.split('/')[-1]   # save handle
                elif value_type.endswith('gitlab_project'):
                    metadata['gitlab_group'] = child.find('text').text
                elif value_type.endswith('research_field/title'):
                    metadata['research_field'] = child.find('option').attrib['{http://purl.org/dc/elements/1.1/}uri']

        return metadata

